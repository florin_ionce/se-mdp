import java.util.ArrayList;

/**
 * Created by tudor on 1/11/15.
 */
public class AlgorithmS1min {

    public static ArrayList<State> output= new ArrayList();
    public static ArrayList<State> R= new ArrayList();
    public static ArrayList<State> Rprim = new ArrayList();
    public static ArrayList<State> S= new ArrayList();


    public ArrayList<State> s1min(Mdp input, ArrayList<State> rezult ){


        for (int i = 0;i< input.states.size();i++){
            S.add(input.states.get(i));                 //added states before making R=S-S0
            R.add(input.states.get(i));
        }

        R.removeAll(rezult);

        ArrayList<State> buffer = (ArrayList)input.states;

        do{
            Rprim=R;

            for(int i = 0; i < Rprim.size(); i++){
                for(int j=0;j<buffer.size();j++){
                    State var=input.getEdges().get(j).hasEdge(Rprim.get(i));
                    if (!(var==null) && (S.contains(var)) && Rprim.contains(var)){
                        R.remove(Rprim.get(i));
                    }
                }
            }

        }while (!Rprim.equals(R));

        return R;

    }

}