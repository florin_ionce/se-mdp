import java.util.ArrayList;

public class Edge {

    int reward;
    double probability;
    String label;
    State start, end;

    public Edge(String label, int  reward, double probability, State start, State end){
        this.label=label;
        this.reward=reward;
        this.probability=probability;
        this.start=start;
        this.end=end;
    }

    public State hasEdge(State s1){

        if(s1.equals(this.start)){
            return this.end;
        }
        if(s1.equals(this.end)){
            return this.start;
        }

        return null;

    }


    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getReward() {
        return reward;
    }

    public void setReward(int reward) {
        this.reward = reward;
    }

    public double getProbability() {
        return probability;
    }

    public State getStart() {
        return start;
    }

    public void setStart(State start) {
        this.start = start;
    }

    public State getEnd() {
        return end;
    }

    public void setEnd(State end) {
        this.end = end;
    }

    public void setProbability(double probability) {
        this.probability = probability;
    }

    public void setProbability(int probability) {
        this.probability = probability;
    }

}
