import java.util.ArrayList;
import java.util.List;


public class Mdp {

    public List<State> states = new ArrayList<State>();
    public List<Edge> edges = new ArrayList<Edge>();

    public Mdp(List<State> states, List<Edge> edges){
        this.states=states;
        this.edges=edges;
    }

    @Override
    public String toString() {
        return "Mdp [states=" + states + ", edges=" + edges + "]";
    }

    public List<State> getStates() {
        return states;
    }
    public void setStates(List<State> states) {
        this.states = states;
    }
    public List<Edge> getEdges() {
        return edges;
    }
    public void setEdges(List<Edge> edges) {
        this.edges = edges;
    }


}
