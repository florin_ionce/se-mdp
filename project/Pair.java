/**
 * Created by sky on 1/12/15.
 */
public class Pair {
    private State s;
    private Double probability;

    public Pair(State s, Double probability) {
        this.s = s;
        this.probability = probability;
    }


    public Double getProbability() {
        return probability;
    }

    public void setProbability(Double probability) {
        this.probability = probability;
    }

    public State getS() {
        return s;
    }

    public void setS(State s) {
        this.s = s;
    }


}
