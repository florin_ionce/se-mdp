import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class Main {

    public static Mdp m;
    private static ArrayList<State> states=new ArrayList();
    private static ArrayList<Edge> edges= new ArrayList();

    public static void main(String[] args)
    {

        State s0 = new State("S0 init", 1);
        State s1 = new State("S1 nimic", 2);
        State s2 = new State("S2 succ", 0);
        State s3 = new State(" S3 fail", 0);

        Edge e0 = new Edge("go",1,1,s0,s1);
        Edge e1 = new Edge("safe",1,0.7,s1,s0);
        Edge e2 = new Edge("safe",1,0.3,s1,s2);
        Edge e3 = new Edge("risk",4,0.5,s1,s2);
        Edge e4 = new Edge("risk",4,0.5,s1,s3);
        Edge e5 = new Edge("reset",5,1,s3,s1);
        Edge e6 = new Edge("finish",0,1,s2,s2);
        Edge e7 = new Edge("stop",0,1,s3,s3);


        states.add(s0);
        states.add(s1);
        states.add(s2);
        states.add(s3);

        edges.add(e0);
        edges.add(e1);
        edges.add(e2);
        edges.add(e3);
        edges.add(e4);
        edges.add(e5);
        edges.add(e6);
        edges.add(e7);

        m = new Mdp(states,edges);
        Algorithm min = new Algorithm();
        AlgorithmS1min max = new AlgorithmS1min();
        GaussSiedel gs = new GaussSiedel();

        System.out.println();


        ArrayList<State> s0Min = min.s0min(m);
        System.out.println("Results");
        for(int i = 0; i < s0Min.size(); i++ ){
            System.out.println("    S0min["+ i + "]= " + s0Min.get(i).getLabel());
    }


        ArrayList<State> s1Min = max.s1min(m, s0Min);
        for(int i = 0; i < s1Min.size(); i++){
            System.out.println("    S1min[" + i + "]= " + s1Min.get(i).getLabel());
        }

        System.out.println("Gauss-Siedel Solution:");

        double x[] = gs.sloveGaussSiedel(m,s0Min,s1Min, 0.3);
        for(int i = 0; i < x.length; i++){
            System.out.println("    x[" + i + "]= " + x[i]);
        }

    }



}
