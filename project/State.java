
public class State {

    String label;
    int reward;
    boolean added;

    public State(String label, int reward){

        this.label=label;
        this.reward=reward;
        this.added=false;


    }



    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getReward() {
        return reward;
    }

    public void setReward(int reward) {
        this.reward = reward;
    }

}
