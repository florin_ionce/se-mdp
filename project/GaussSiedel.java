import com.sun.swing.internal.plaf.synth.resources.synth_sv;
import scala.Predef;

import java.util.*;

/**
 * Created by sky on 1/11/15.
 */
public class GaussSiedel {

    ArrayList<State> S;
    ArrayList<Edge> E;

    public double[] sloveGaussSiedel(Mdp input, ArrayList<State> s0min, ArrayList<State> s1min, double criterion ){

        S = (ArrayList) input.getStates();
        E = (ArrayList) input.getEdges();

        ArrayList<State> sDiff = makeDiffFromStates(S, s0min,s1min);

        double[] x = new double[S.size()];
        x = initializeArray(x, S, s1min);

        double sigma  = 0;

        do{
            sigma = 0;
            for(State s: sDiff){
                double xNew = computeNewX(s,x);
                int poz = S.indexOf(s);
                sigma =  max(sigma, (xNew - x[poz]));
                x[poz] = xNew;
            }
        }while(sigma > criterion);

        return x;
    }


    private double max(double m1, double m2){
        if(m1 > m2)
            return m1;
        return m2;
    }

    private double computeNewX(State s, double[] x){
        ArrayList<Edge> actions = getAllActionsFrom(s, E);

        ArrayList<Pair> probabilites;
        double min = 10;
        double sumResult = 0;
        for(Edge e: actions){
            probabilites = getStatesAndProbabilities(s,E, e.getLabel());
            sumResult = 0;

            for(Pair p: probabilites){
                double prob = p.getProbability();
                int position = S.indexOf(p.getS());
                sumResult += prob * x[position];
            }
        }

        if (sumResult < min ){
            min = sumResult;
        }

        return min;
    }


    private ArrayList<State> makeDiffFromStates(ArrayList<State> S, ArrayList<State> s0min, ArrayList<State> s1min){
        ArrayList<State> buffer = new ArrayList<State>();
        for(State s : S){
            if(!s0min.contains(s) && !s1min.contains(s)){
                buffer.add(s);
            }
        }
        return buffer;
    }

    private double[] initializeArray(double[] x, ArrayList<State> S, ArrayList<State>s1Min){
        int i = 0;
        for(State s: S){
            if(s1Min.contains(s)){
                x[i] = 1;
            }else{
                x[i] = 0;
            }
            i++;
        }
        return x;
    }


    /**
     *
     * @param s
     * @param edges
     * @return an array of edges that start form the stat @param s
     */
    private ArrayList<Edge> getAllActionsFrom(State s,  ArrayList<Edge> edges){

        ArrayList<Edge> buffer = new ArrayList<Edge>();

        for(Edge e : edges){
            if (e.getStart().equals(s)){
                buffer.add(e);
            }
        }
        return buffer;
    }


    /**
     *
     * @param s
     * @param edges
     * @param action
     * @return a set of pairs containing the probability and the approachable that start from the State @param s
     * with the action @param action
     */
    private ArrayList<Pair>  getStatesAndProbabilities(State s, ArrayList<Edge> edges, String action){

        ArrayList<Pair> buff = new ArrayList<Pair>();

        for(Edge e: edges){
            if (e.getLabel().equals(action) && e.getStart().equals(s)){
             buff.add(new Pair(e.getEnd(), e.getProbability()));
            }
        }

        return buff;

    }
}
